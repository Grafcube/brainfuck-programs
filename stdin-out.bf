[Shamelessly ripped off from https://gist.github.com/roachhd/dce54bec8ba55fb17d3a and some stackoverflow post. Kind of.
Specifically the STDIN and STDOUT unix cat thing. And no copy-pasting.]

>                           Leaving the first cell at 0 to be able to jump to the start with a loop
,                           Read from STDIN
----------                  Check for \n
[++++++++++>,----------]    Loop that keeps reading until it reaches \n
++++++++++                  Restore \n
<[<]                        Go back to the beginning
>[.>]                       Print the string
